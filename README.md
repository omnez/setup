# My automation script

Repository can be found in https://gitlab.com/omnez/setup

## Usage

`bash -c "$(curl -fsSL setup.onnilampi.fi)"`

## Included files

This repository also contains files that are not used and installed automatically.
Here is a manifest of those files and their original sources:
- [LaTeX indent rules for vim](files/tex.vim) - https://www.vim.org/scripts/script.php?script_id=218
