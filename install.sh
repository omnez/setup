#!/bin/bash

echo "This is an automated script designed to install necessary configuration files as they are used by Onni Lampi."
echo "See https://gitlab.com/omnez/setup for more details"

# Use colors, but only if connected to a terminal, and that terminal
# supports them.
# Also check that git, vim, zsh are installed.
# Shamelessly copied from oh-my-zsh installation script
if which tput >/dev/null 2>&1; then
    ncolors=$(tput colors)
fi
if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
fi

CHECK_ZSH_INSTALLED=$(grep /zsh$ /etc/shells | wc -l)
if [ ! $CHECK_ZSH_INSTALLED -ge 1 ]; then
  printf "${YELLOW}Zsh is not installed!${NORMAL} Please install zsh first!\n"
  exit
fi
unset CHECK_ZSH_INSTALLED

which vim &> /dev/null
CHECK_VIM_INSTALLED=$?
if [ $CHECK_VIM_INSTALLED -eq 1 ]; then
  printf "${YELLOW}vim is not installed!${NORMAL} Please install vim first!\n"
  exit
fi
unset CHECK_VIM_INSTALLED

which git &> /dev/null
CHECK_GIT_INSTALLED=$?
if [ $CHECK_GIT_INSTALLED -eq 1 ]; then
  printf "${YELLOW}git is not installed!${NORMAL} Please install git first!\n"
  exit
fi
unset CHECK_GIT_INSTALLED

# End of copied part
#

if [[ $EUID -ne 0 ]]
then
    bash -c "$(curl -fsSL https://gitlab.com/omnez/setup/raw/master/zsh-install.sh)"
    echo "${BOLD}Installing Vundle...${NORMAL}"
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim 2> /dev/null
    if [ $? == 0 ]
    then
        echo "${GREEN}Installed Vundle${NORMAL}"
    else
        echo "${YELLOW}Vundle is already installed.${NORMAL}"
    fi
    echo "${BOLD}Installing .vimrc...${NORMAL}"
    curl -fsSL https://gitlab.com/omnez/setup/raw/master/.vimrc -o ~/.vimrc
    if [ $? == 0 ]
    then
        echo "${GREEN}.vimrc installed!${NORMAL}"
    else
        echo "${RED}Something happened. Check permissions!${NORMAL}"
        exit 1
    fi
    echo "${BOLD}Installing vim plugins...${NORMAL}"
    vim +PluginInstall +qall
    if [ $? == 0 ]
    then
        echo "${GREEN}Vim plugins installed!${NORMAL}"
    else
        echo "${RED}Something happened. Check permissions!${NORMAL}"
        exit 1
    fi
    echo "${BOLD}Installing .zshrc and custom zsh theme...${NORMAL}"
    curl -fsSL https://gitlab.com/omnez/setup/raw/master/.zshrc -o ~/.zshrc
    if [ $? == 0 ]
    then
        echo "${GREEN}.zshrc installed!${NORMAL}"
    else
        echo "${RED}Something happened. Check permissions!${NORMAL}"
        exit 1
    fi
    curl -fsSL https://gitlab.com/omnez/setup/raw/master/omnez.zsh-theme -o ~/.oh-my-zsh/themes/omnez.zsh-theme
    if [ $? == 0 ]
    then
        echo "${GREEN}Custom zsh theme installed!${NORMAL}"
    else
        echo "${RED}Something happened. Check permissions!${NORMAL}"
        exit 1
    fi
    echo "${BOLD}Checking whether a .custrc exists in the installation...${NORMAL}"
    if [ -e ~/.custrc ]
    then
        echo "${YELLOW}.custrc exists, won't touch it${NORMAL}"
    else
        echo "${GREEN}Creating a dummy .custrc${NORMAL}"
        CREATED_ON="${date}"
        echo "#This is a dummy file created by setup.onnilampi.fi on `date` " > ~/.custrc
    fi
else
    echo "Running as root, re-run script on another user"
    echo "Exiting..."
fi

